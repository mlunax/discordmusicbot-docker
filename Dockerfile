FROM openjdk:11
WORKDIR /app
COPY files/JMusicBot.jar .
CMD ["java", "-Dnogui=true", "-jar", "JMusicBot.jar"]