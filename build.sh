# function from https://gist.github.com/lukechilds/a83e1d7127b78fef38c2914c4ececc3c
get_latest_release() {
  curl --silent "https://api.github.com/repos/$1/releases/latest" | # Get latest release from GitHub api
    grep '"tag_name":' |                                            # Get tag line
    sed -E 's/.*"([^"]+)".*/\1/'                                    # Pluck JSON value
}
TAG=$(get_latest_release "jagrosh/MusicBot")
[ -f files/JMusicBot.jar ] || wget https://github.com/jagrosh/MusicBot/releases/download/$TAG/JMusicBot-$TAG.jar -O files/JMusicBot.jar
# mv JMusicBot-$TAG.jar JMusicBot.jar
export COMPOSE_DOCKER_CLI_BUILD=1 
export DOCKER_BUILDKIT=1
docker-compose build || docker compose build
mkdir -p configs
touch configs/serversettings.json configs/config.txt