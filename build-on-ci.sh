# function from https://gist.github.com/lukechilds/a83e1d7127b78fef38c2914c4ececc3c
get_latest_release() {
  curl --silent "https://api.github.com/repos/$1/releases/latest" | # Get latest release from GitHub api
    grep '"tag_name":' |                                            # Get tag line
    sed -E 's/.*"([^"]+)".*/\1/'                                    # Pluck JSON value
}
TAG=$(get_latest_release "jagrosh/MusicBot")
[ -f files/JMusicBot.jar ] || wget https://github.com/jagrosh/MusicBot/releases/download/$TAG/JMusicBot-$TAG.jar -O files/JMusicBot.jar
# mv JMusicBot-$TAG.jar JMusicBot.jar

# if [[ "$CI_COMMIT_BRANCH" == "$CI_DEFAULT_BRANCH" ]]; then
#         tag=""
#         echo "Running on default branch '$CI_DEFAULT_BRANCH': tag = 'latest'"
#       else
#         tag=":$CI_COMMIT_REF_SLUG"
#         echo "Running on branch '$CI_COMMIT_BRANCH': tag = $tag"
#       fi
# docker build --pull -t "$CI_REGISTRY_IMAGE${tag}" .
# docker push "$CI_REGISTRY_IMAGE${tag}"


# kaniko
# mkdir -p /kaniko/.docker
# echo "{\"auths\":{\"${CI_REGISTRY}\":{\"auth\":\"$(printf "%s:%s" "${CI_REGISTRY_USER}" "${CI_REGISTRY_PASSWORD}" | base64 | tr -d '\n')\"}}}" > /kaniko/.docker/config.json
# /kaniko/executor
# --context "${CI_PROJECT_DIR}"
# --dockerfile "${CI_PROJECT_DIR}/Dockerfile"
# --destination "${CI_REGISTRY_IMAGE}:${CI_COMMIT_TAG}"
